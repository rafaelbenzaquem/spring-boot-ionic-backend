package com.rafaelbenzaquem.cursomc.repositories;

import com.rafaelbenzaquem.cursomc.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente,Integer>{
}
