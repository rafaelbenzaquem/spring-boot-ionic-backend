package com.rafaelbenzaquem.cursomc.repositories;

import com.rafaelbenzaquem.cursomc.domain.Endereco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco,Integer>{
}
