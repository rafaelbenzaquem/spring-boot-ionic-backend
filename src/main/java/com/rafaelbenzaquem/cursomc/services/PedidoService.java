package com.rafaelbenzaquem.cursomc.services;

import com.rafaelbenzaquem.cursomc.domain.ItemPedido;
import com.rafaelbenzaquem.cursomc.domain.PagamentoComBoleto;
import com.rafaelbenzaquem.cursomc.domain.Pedido;
import com.rafaelbenzaquem.cursomc.domain.enums.EstadoPagamento;
import com.rafaelbenzaquem.cursomc.repositories.ItemPedidoRepository;
import com.rafaelbenzaquem.cursomc.repositories.PagamentoRepository;
import com.rafaelbenzaquem.cursomc.repositories.PedidoRepository;
import com.rafaelbenzaquem.cursomc.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PedidoService {

    @Autowired
    PedidoRepository pedidoRepository;

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    ItemPedidoRepository itemPedidoRepository;

    @Autowired
    BoletoService boletoService;

    @Autowired
    ProdutoService produtoService;

    public Pedido find(Integer id){
        return pedidoRepository.findById(id).orElseThrow(() -> new ObjectNotFoundException(
                "Objeto não encontrado! Id: " + id + ", Tipo: " + Pedido.class.getSimpleName()));
    }

    public List<Pedido> findAll(){
        return pedidoRepository.findAll();
    }

    public Pedido insert(Pedido pedido) {
        pedido.setId(null);
        pedido.setInstante(new Date());
        pedido.getPagamento().setEstado(EstadoPagamento.PENDENTE);
        pedido.getPagamento().setPedido(pedido);


        if(pedido.getPagamento() instanceof PagamentoComBoleto){
            PagamentoComBoleto pagamento = (PagamentoComBoleto) pedido.getPagamento();
            boletoService.preencherPagamentoComBoleto(pagamento,pedido.getInstante());
        }
        pedido = pedidoRepository.save(pedido);
        pedido.setPagamento(pagamentoRepository.save(pedido.getPagamento()));
        for(ItemPedido item : pedido.getItens()){
            item.setDesconto(0.0);
            item.setPreco(item.getProduto().getPreco());
            item.setPedido(pedido);
        }
        itemPedidoRepository.saveAll(pedido.getItens());
        return pedido;
    }
}
