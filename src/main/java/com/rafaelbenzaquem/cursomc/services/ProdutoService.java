package com.rafaelbenzaquem.cursomc.services;

import com.rafaelbenzaquem.cursomc.domain.Categoria;
import com.rafaelbenzaquem.cursomc.domain.Produto;
import com.rafaelbenzaquem.cursomc.repositories.CategoriaRepository;
import com.rafaelbenzaquem.cursomc.repositories.ProdutoRepository;
import com.rafaelbenzaquem.cursomc.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProdutoService {

    @Autowired
    ProdutoRepository produtoRepository;

    public Produto find(Integer id){
        return produtoRepository.findById(id).orElseThrow(() -> new ObjectNotFoundException(
                "Objeto não encontrado! Id: " + id + ", Tipo: " + Produto.class.getSimpleName()));
    }

    public Page<Produto> search(String nome, List<Categoria> categorias,Integer page, Integer linesPerPage, String direction, String... properties) {
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), properties);
        return produtoRepository.findDistinctByNomeContainingAndCategoriasIn(nome,categorias,pageRequest);
    }

    public List<Produto> findAll(){
        return produtoRepository.findAll();
    }

}
