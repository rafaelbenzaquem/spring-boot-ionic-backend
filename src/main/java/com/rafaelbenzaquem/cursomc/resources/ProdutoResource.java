package com.rafaelbenzaquem.cursomc.resources;

import com.rafaelbenzaquem.cursomc.domain.Categoria;
import com.rafaelbenzaquem.cursomc.domain.Produto;
import com.rafaelbenzaquem.cursomc.domain.Produto;
import com.rafaelbenzaquem.cursomc.dto.ProdutoDTO;
import com.rafaelbenzaquem.cursomc.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/produtos")
public class ProdutoResource {

    @Autowired
    ProdutoService service;

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    public ResponseEntity<List<Produto>> findAll(){
        return ResponseEntity.ok().body(service.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Produto> find(@PathVariable Integer id){
        Produto produto =  service.find(id);
        return ResponseEntity.ok().body(produto);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page<ProdutoDTO>> findPage(
            @RequestParam(value = "nome",defaultValue = "") String nome,
            @RequestParam(value = "categorias",defaultValue = "") String categorias,
            @RequestParam(value = "page",defaultValue = "0") Integer page,
            @RequestParam(value = "linesPerPage",defaultValue = "24") Integer linesPerPage,
            @RequestParam(value = "direction",defaultValue = "ASC") String direction,
            @RequestParam(value = "orderBy",defaultValue = "nome") String orderBy
    ){

        try {
            nome = URLDecoder.decode(nome,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Page<Produto> pages = service.search(nome,urlToCategorias(categorias),page,linesPerPage,direction,orderBy);
        Page<ProdutoDTO> produtos = pages.map(p -> new ProdutoDTO(p));
        return ResponseEntity.ok().body(produtos);
    }

    private List<Categoria> urlToCategorias(String urlResquestParameter){
//        List<Categoria> categorias = new ArrayList<>();
//        Arrays.asList(urlResquestParameter.split(",")).forEach(id-> categorias.add(new Categoria(Integer.parseInt(id),null)));
//        return categorias;
        return Arrays.asList(urlResquestParameter.split(",")).stream().map(id -> new Categoria(Integer.parseInt(id),null)).collect(Collectors.toList());
    }

}
