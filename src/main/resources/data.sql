insert into categoria(nome) values ('informática');
insert into categoria(nome) values ('escritório');
insert into categoria(nome) values ('cama, mesa e banho');
insert into categoria(nome) values ('eletrônicos');
insert into categoria(nome) values ('jardinagem');
insert into categoria(nome) values ('decoração');
insert into categoria(nome) values ('perfumaria');
insert into categoria(nome) values ('importados');

insert into produto(nome,preco) values('computador',2000.00);
insert into produto(nome,preco) values('impressora',800.00);
insert into produto(nome,preco) values('mouse',80.00);
insert into produto(nome,preco) values('mesa de escritório',300.00);
insert into produto(nome,preco) values('toalha',50.00);
insert into produto(nome,preco) values('colcha',200.00);
insert into produto(nome,preco) values('tv true color',1200.00);
insert into produto(nome,preco) values('roçadeira',800.00);
insert into produto(nome,preco) values('abajour',100.00);
insert into produto(nome,preco) values('pendente',180.00);
insert into produto(nome,preco) values('shampoo',28.00);

insert into produto_categoria(produto_id,categoria_id) values(1,1);
insert into produto_categoria(produto_id,categoria_id) values(2,1);
insert into produto_categoria(produto_id,categoria_id) values(3,1);
insert into produto_categoria(produto_id,categoria_id) values(2,2);
insert into produto_categoria(produto_id,categoria_id) values(4,2);
insert into produto_categoria(produto_id,categoria_id) values(5,3);
insert into produto_categoria(produto_id,categoria_id) values(6,3);
insert into produto_categoria(produto_id,categoria_id) values(1,4);
insert into produto_categoria(produto_id,categoria_id) values(2,4);
insert into produto_categoria(produto_id,categoria_id) values(3,4);
insert into produto_categoria(produto_id,categoria_id) values(7,4);
insert into produto_categoria(produto_id,categoria_id) values(8,5);
insert into produto_categoria(produto_id,categoria_id) values(9,6);
insert into produto_categoria(produto_id,categoria_id) values(10,6);
insert into produto_categoria(produto_id,categoria_id) values(11,7);

insert into estado(nome) values('minas gerais');
insert into estado(nome) values('são paulo');

insert into cidade(nome,estado_id) values ('uberlândia',1);
insert into cidade(nome,estado_id) values ('são paulo',2);
insert into cidade(nome,estado_id) values ('campinas',2);

insert into cliente(nome,email,cpf_ou_cnpj,tipo) values ('maria silva','maria@gmail.com','36378912377',1);

insert into telefone(cliente_id,numero) values (1,'27363323');
insert into telefone(cliente_id,numero) values (1,'93838393');

insert into endereco(logradouro,numero,complemento,bairro,cep,cliente_id,cidade_id)
values('rua flores','300','apto 203','jardim','36220634',1,1);
insert into endereco(logradouro,numero,complemento,bairro,cep,cliente_id,cidade_id)
values('avenida matos','105','sala 800','centro','38777012',1,2);

insert into pedido(instante,cliente_id,endereco_de_entrega_id) values ('2017-09-30 23:58:00.000',1,1);
insert into pagamento(pedido_id,estado)values (1,2);
insert into pagamento_com_cartao(numero_parcelas,pedido_id)values (6,1);

insert into pedido(instante,cliente_id,endereco_de_entrega_id) values ('2017-10-10 19:35:00.000',1,2);
insert into pagamento(pedido_id,estado)values (2,1);
insert into pagamento_com_boleto(data_vencimento,pedido_id)values ('2017-10-20',2);

insert into item_pedido(desconto,preco,quantidade,produto_id,pedido_id) values (0.00   ,2000.00 ,1, 1, 1);
insert into item_pedido(desconto,preco,quantidade,produto_id,pedido_id) values (0.00   ,80.00   ,2, 3, 1);
insert into item_pedido(desconto,preco,quantidade,produto_id,pedido_id) values (100.00 ,800.00  ,1, 2, 2);
